<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/all', function () {
    $users = \App\User::all();
    $list = $users->map(function($user){

        return $user->name.' rsvp:'.$user->rsvp.' password:'. $user->password;
    });
    dd($list);
});

Route::get('/reset', function () {
    $prefixes = \App\Prefix::where('active', 1);
    $suffixes = \App\Suffix::where('active', 1);
    $suffixes->update(['active' => 0]);
    $prefixes->update(['active' => 0]);

    $users = \App\User::where('staff', 1)->get();

    dd($users);
});

Route::get('/all-codes', function () {
    $prefixes = \App\Prefix::where('active', 1);
    $suffixes = \App\Suffix::where('active', 1);

    $suffixes->update(['active' => 0]);
    $prefixes->update(['active' => 0]);


    $prefixes = \App\Prefix::all();

    $list = $prefixes->map(function($item) {

        $suffix = \App\Suffix::where('active', false)->get()->random();
        $prefix = \App\Prefix::where('id', $item->id)->get()->random();

        $suffix->active = $prefix->active = true;
        $suffix->save();
        $suffix->save();

        return $item->name . ' ' . $suffix->name;
    });

    dd($list);
});

Route::get('/rsvp/{id}', function ($id) {
    $user = \App\User::find($id);

    if(!$user) {
        abort(404);
    }

    if(!$user->password || $user->password === 'temp') {
        $prefix = \App\Prefix::where('active', 0)->get()->random();
        $suffix = \App\Suffix::where('active', 0)->get()->random();

        $user->prefix_id = $prefix->id;
        $user->suffix_id = $suffix->id;
        $user->password = $prefix->name.' '.$suffix->name;

        $suffix->active = $prefix->active = 1;

        $user->save();
        $suffix->save();
        $prefix->save();
    }

    return view('rsvp')->with(['user' => $user ] );
});

Route::post('/rsvp/{user}/plus-one',  'UsersController@add' )->name('add.member');
Route::put('/rsvp/{user}/confirm',  'UsersController@confirm' )->name('confirm.member');

Route::get('/reset-rsvp', function () {
    $users = \App\User::all();
    $users->update([
        'password' => NULL,
        'prefix_id' => NULL,
        'suffix_id' => NULL,
    ]);
    dd($users);
});
