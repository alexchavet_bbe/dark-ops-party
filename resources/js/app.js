
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('blacked-out', require('./components/BlackOut.vue'));
Vue.component('do-countdown', require('./components/countdown.vue'));

const app = new Vue({
    el: '#app',

    mounted() {
        const blackedOuts = this.$root.$children.filter(child => { return child.$options.name === "blacked-out" && child.reveal; });

        var i = 0;
        var interval = setInterval(function() {
            var art = blackedOuts[i++];
            art.visible = !art.visible;
            art.typeWriter();
            if(i >= blackedOuts.length) clearInterval(interval);
        }, 1000);
        console.log(blackedOuts);
    }
});
