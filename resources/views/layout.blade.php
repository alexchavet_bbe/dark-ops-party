<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>DARK OPS PARTY</title>
<link rel="stylesheet" href="{{ mix('/css/app.css') }}">
<!-- Fonts -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD_OOiOTUp989NVktM7d_eeRAfqqw0nyS0&libraries=geometry,places"></script>
</head>
<body>
<div id="app" :class="classList">

        @yield('content')

</div>
<script src="{{ mix('/js/app.js')}}"></script>
</body>
</html>
