<div class="row justify-content-center mt-5">
    @if($user->rsvp)
        <div class="col-lg-8 text-center mb-4">
            <h2 class="text-uppercase">You are enlisted</h2>
            <p>Party starts in</p>
            <p><do-countdown></do-countdown></p>
        </div>
    @else
        <div class="col-lg-8 text-center">

            <form class="d-flex flex-column justify-content-center align-items-center" action="{{ route('confirm.member', $user->id) }}" method="POST">
                @method('PUT')
                @csrf
                <button  class="btn btn-primary btn-lg text-uppercase" type="submit">Enlist now</button>

            </form>
        </div>
    @endif
    @if($user->rsvp && $user->staff && !$user->plus_one)
        <div class="col-lg-8">
            <form action="{{ route('add.member', $user) }}" method="POST" class="d-flex flex-column justify-content-center align-items-center">
                @csrf
                <h2 class="text-uppercase">Recruit a plus one</h2>
                <p>Who do you trust?</p>
                <div class="form-group form-inline">
                    {{--<label for="exampleInputEmail1">First name</label>--}}
                    <input name="name" type="text" class="form-control" id="exampleInputEmail1"
                           aria-describedby="emailHelp" placeholder="Enter first name">
                </div>

                <div class="form-group form-inline">
                    {{--<label for="exampleInputEmail1">Email address</label>--}}
                    <input name="email" type="email" class="form-control" id="exampleInputEmail1"
                           aria-describedby="emailHelp" placeholder="Enter email">
                </div>

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <button type="submit" class="btn btn-primary">Recruit</button>
            </form>
        </div>
    @endif
    @if($user->plus_one)
            <div class="col-lg-8 text-center">
        Your plus one we will be contacted soon
            </div>
    @endif
</div>
