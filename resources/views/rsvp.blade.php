@extends('layout')

@section('content')
    <div class="container">

        <div class="do-header row justify-content-center align-items-center my-5">

            <div class="col-lg-2 col-sm-4 d-block text-sm-left text-center">
                <img src="{{asset('images/logo.png')}}" alt="">
            </div>
            <div class="col-lg-2 col-sm-4 text-center my-sm-0 my-4">
                <img class=" d-inline-block bbe-logo" src="{{asset('images/bbe.png')}}" alt=""><br>
                <img class=" d-inline-block" src="{{asset('images/classified.png')}}" alt="">
            </div>
            <div class="col-lg-2 col-sm-4 text-sm-right text-center">

            </div>
        </div>

        <div class="row justify-content-center mt-4">
            <div class="col-lg-6">
                TO: <blacked-out text="{{ $user->name }}"></blacked-out>&nbspa.k.a <blacked-out text="{{ $user->password }}"></blacked-out>
                <br>FROM: <blacked-out text="B.B.E"></blacked-out>
                <br>RE: You have been selected for <blacked-out text="B.B.E’s Dark Ops"></blacked-out>
            </div>
        </div>

        <div class="row justify-content-center mt-4">
            <div class="col-lg-6">
                <p>
                    Pursuant to the request of the directors <blacked-out :reveal="false" text="‘black ’"></blacked-out> &nbsp;
                    your presence is required at the <blacked-out :reveal="false" text="‘black black black’"></blacked-out><span>&nbsp;</span>party.<span>&nbsp;</span>
                    <em><blacked-out text="{{ $user->name }}"></blacked-out></em>&nbsp;no longer exists. Let <em><blacked-out text="{{ $user->password }}"></blacked-out></em><span>&nbsp;</span>take control and party like it's <blacked-out :reveal="false" text="‘black black black’"></blacked-out>
                </p>


                DATE: 21.09.2018
                <br>TIME: <blacked-out text="1900"></blacked-out>
                <br>LOCATION: <blacked-out text="Shifty’s, 375 Brunswick St, Fitzroy"></blacked-out>
                <br>UNIFORM: <blacked-out text="Dark Ops"></blacked-out>
            </div>
        </div>

        @include('partials.add-plus-one')

        <div class="do-header row justify-content-center align-items-center mt-4">

            <div class="col-lg-3 col-6 d-block text-sm-left text-center">
                <img src="{{asset('images/reviewd.png')}}" alt="">
            </div>
            <div class="col-lg-3 col-6 text-sm-right text-center">
                <img class=" d-inline-block" src="{{asset('images/signature.png')}}" alt="">
            </div>
        </div>

        <div class="do-header row justify-content-center align-items-center mt-4 pb-5">
            <div class="col text-center">
                <img src="{{asset('images/footer.png')}}" alt="">
            </div>
        </div>

    </div>
@endsection
