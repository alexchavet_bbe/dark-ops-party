<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LinkUserPrefix extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedInteger('prefix_id')->nullable();
            $table->foreign('prefix_id')->references('id')->on('prefixes');

            $table->unsignedInteger('suffix_id')->nullable();
            $table->foreign('suffix_id')->references('id')->on('suffixes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('prefix_id');
            $table->dropColumn('suffix_id');
        });
    }
}
