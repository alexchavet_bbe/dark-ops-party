<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePlusOneRequest;
use App\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function add(User $user, StorePlusOneRequest $request)
    {
        $plus_one = new User;
        $plus_one->name = $request->name;
        $plus_one->email = $request->email;
        $plus_one->password = 'temp';
        $plus_one->save();

        $user->plusOne()->associate($plus_one)->save();

        return redirect()->back();
    }

    public function confirm(User $user)
    {

        $user->rsvp = true;
        $user->save();

        return redirect()->back();
    }
}
